

((sym_name) @label
  (#lua-match? @label "#"))

((sym_name) @keyword
    (#any-of? @keyword "define" "set" "let" "require"))

((sym_name) @operator
    (#any-of?
        @operator
        "+" "-" "*" "/" "fdiv" "**" ">" "<" "=" "<=" ">=" "&&" "||"))


((sym_name) @function.builtin
    (#any-of?
        @function.builtin
        "print" "printf" "call" "import"
        "list" "first" "second"
        "slice" "array" "map" "mapa"
        "eval" "quote" "get" "seta" "geta"
        "whenever" "find" "count" "groups" "in-group" "in-groups"
        "in-scope" "in-scopes" "resolve-group" "resolve-scope"
        "atom?" "fold" "int?" "range" "symbol?"
        "average" "fold/signal" "lambda" "type"
        "call" "for" "last" "resolve-group"
        "unalias" "case" "get" "length"
        "resolve-scope" "cond" "geta"
        "unload" "convert/bin" "geta/default"
        "letret" "reval" "unset-scope" "count"
        "list" "second" "when"
        "help" "list?" "whenever"
        "do" "load" "set-scope"
        "map"
        "zip" "exit" "in"
        "mapa" "slice"
        "in-group" "max" "step"
        "in-groups" "min" "string->int" "alias" "find/g"
        "in-scope" "print" "string?" "all-scopes" "first"
        "inc" "printf" "sum" "array" "fn"
        "int->string" "symbol->string"))


((sym_name) @conditional
    (#any-of?
        @conditional
        "if" "cond" "when" "unless"))


((sym_name) @conditional
    (#any-of?
        @conditional
        "for" "while" "whenever"))

(group_lit) @label

(syn_quoting_lit) @walQuoted
(unquoting_lit) @walUnquoted
(unquote_splicing_lit) @walUnquoted

[
 "defmacro"
 "defun"
] @keyword

(parameter (sym_lit) @parameter)


;; Builtin functions

(num_lit) @number

[
  (char_lit)
  (str_lit)
] @string

[
 (bool_lit)
 (nil_lit)
] @constant.builtin

(kwd_lit) @constant

;; Comments

(comment) @comment

